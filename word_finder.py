"""
A CLI tool that finds a phrase (single word) in a given text file.
Search phrase (single word) can be misspelled.
American Soundex algorithm is used to match words.
Top 5 unique matched words are returned.
"""


import string
import re
import difflib
import argparse
import sys
import os
import multiprocessing
import itertools
import pandas as pd
import numpy as np


def clean_up_chars(a_string: str) -> str:
    """
    Removes punctuation and escape sequence characters from a string.

    Args:
        a_string: a string with special characters.

    Returns:
        A string without punctuation and escape sequence characters.

    """

    # punctuation characters are removed:
    pun_chars = string.punctuation
    tr_table = str.maketrans(pun_chars, len(pun_chars) * ' ')
    a_string = a_string.translate(tr_table)

    # escape sequences are removed
    escape_chars = ['\\', '\a', '\b', '\f', '\n', '\r', '\t', '\v']
    for char in escape_chars:
        a_string = a_string.replace(char, ' ')

    return a_string


def replace_cons(a_string: str) -> str:
    """
    Replaces consonants in a string with numbers.

    Args:
        a_string: a string with consonants.

    Returns:
        A string with consonants replaced with numbers.

    """

    cons_num = {1: 'bfpv', 2: 'cgjkqsxz', 3: 'dt', 4: 'l', 5: 'mn', 6: 'r'}

    for key in cons_num:
        tr_table = str.maketrans(cons_num[key], len(cons_num[key]) * str(key))
        a_string = a_string.lower().translate(tr_table)

    return a_string


def encode_soundex(a_string: str) -> str:
    """
    Encodes a string of a single word using American Soundex.

    Args:
        a_string: a string that contains a single word.

    Returns:
        A string as an American Soundex code (such as P123).

    """

    # regex objects are initiated
    reg_double_digits = re.compile(r'11|22|33|44|55|66')
    reg_sep_w_h = re.compile(r'1[hw]1|2[hw]2|3[hw]3|4[hw]4|5[hw]5|6[hw]6')
    reg_vowels_and_w_h = re.compile(r'[aeiouyhw]')
    ref_soundex_validation = re.compile(r'\w{1}\d{3}')

    encoded = replace_cons(a_string)

    # string start is checked for double digits or dd separated with w or h
    if reg_double_digits.match(encoded):
        encoded = a_string[0] + encoded[2:]
    elif reg_sep_w_h.match(encoded):
        encoded = a_string[0] + encoded[3:]
    else:
        encoded = a_string[0] + encoded[1:]

    # the rest of the string is checked for double digits
    double_digits = reg_double_digits.findall(encoded)
    if double_digits:
        for digit in double_digits:
            encoded = reg_double_digits.sub(digit[0], encoded)

    # the rest of the string is checked for double digits separated with w or h
    double_digits_sep_w_h = reg_sep_w_h.findall(encoded)
    if double_digits_sep_w_h:
        for digit in double_digits_sep_w_h:
            encoded = reg_sep_w_h.sub(digit[0], encoded)

    # the vowels are dropped:
    vowels_and_w_h = reg_vowels_and_w_h.findall(encoded)
    if vowels_and_w_h:
        for _ in vowels_and_w_h:
            encoded = encoded[0] + reg_vowels_and_w_h.sub('', encoded[1:])

    # trailing zeros are added
    encoded = (encoded + '0' * 3)[:4]

    if not ref_soundex_validation.match(encoded):
        encoded = 'x'

    return encoded


def two_str_similarity_ratio(str1: str, str2: str) -> float:
    """
    Calculates the ratio expressing how much one string matches the other.

    Args:
        str1: a single string as the first argument.
        str2: a single string as the second argument.

    Returns:
        A float rounded to the 4th decimal.

    """

    str_match = difflib.SequenceMatcher(None, str1.lower(), str2.lower())
    trailing_ratio = str_match.ratio()

    return round(trailing_ratio, 4)


def create_string_match_matrix(text: str, user_str: str) -> pd.DataFrame:
    """
    Creates a table (matrix) containing match ratios of user and text strings.

    Args:
        text: a string contains multiple words separated with a space char.
        user_str: a string of a single word.

    Returns:
        A Pandas DataFrame sorted by the best match.

    """

    # Unicode characters are removed and user string is encoded
    text_no_unicode_chars = (text.encode('ascii', 'ignore')).decode('utf-8')
    cleaned_str = clean_up_chars(text_no_unicode_chars)
    encoded_usr_str = encode_soundex(user_str)

    # A DatFrame (a.k.a. DF) is initialized
    matrix = pd.DataFrame(np.array(cleaned_str.split(' ')))
    col_1 = 'orig_word'
    col_2 = 'orig_lower'
    col_3 = 'is_empty'
    col_4 = 'encoded_str'
    col_5 = 'usr_str_VS_orig_word'
    col_6 = 'encodedStringVSusrStringCode'
    col_7 = 'score'

    # DF is cleaned from empty and duplicate entries
    matrix.columns = [col_1]
    matrix[col_1] = matrix[col_1].str.strip()
    matrix[col_2] = matrix[col_1].str.lower()
    matrix[col_3] = matrix[col_1] == ''
    matrix = matrix[matrix[col_3] == 0]
    matrix.drop(columns=[col_3], inplace=True)

    # Entries are encoded and compared with the user string
    matrix[col_4] = matrix[col_2].apply(encode_soundex)
    matrix = matrix[matrix[col_4] != 'x']
    matrix.sort_values([col_4, col_1], ascending=True, inplace=True)
    matrix.drop_duplicates(subset=[col_4, col_2], inplace=True)
    matrix[col_5] = matrix.apply(
        lambda x: two_str_similarity_ratio(x[col_2], user_str),
        axis=1)
    matrix[col_6] = matrix.apply(
        lambda x: two_str_similarity_ratio(x[col_4], encoded_usr_str),
        axis=1)

    # Overall match score is asigned and other than top 5 results are dropped.
    matrix.eval(''.join([col_7, '=', col_5, '+', col_6]), inplace=True)
    matrix.sort_values([col_7, col_4, col_1], ascending=False, inplace=True)
    matrix = matrix.head(5)

    return matrix


def get_results(matrix_list: list)-> list:
    """
    Combines multiple Pandas DataFrame objects into one
    and returns the 5 unique top matched words.

    Args:
        matrix_list: a list containing multiple Pandas DataFrame objects.

    Returns:
        A list containing the 5 unique top matched words.

    """

    concat_df = pd.concat(matrix_list, ignore_index=True)
    concat_df.drop_duplicates(inplace=True)
    cols = concat_df.columns
    concat_df.sort_values([cols[-1], cols[2], cols[0]],
                          ascending=False, inplace=True)
    concat_df = concat_df.head(5)
    unique_words = list(concat_df.iloc[:, 0])

    return unique_words


def file_chunks(filename: str) -> list:
    """
    Returns a list of tuples representing file locations.

    Args:
        filename: *.txt file path as the first argument.

    Returns:
        A list containing chunk of files stored as tuples.

    """

    chunk_size = 1024 * 1024 * 100
    chunks = []
    file_size = os.path.getsize(filename)

    if file_size > chunk_size:
        start = 0
        end = chunk_size
        for _ in range(file_size // chunk_size):
            chunks.append((start, end))
            start = end
            end += chunk_size
        if file_size % chunk_size != 0:
            chunks.append((end-chunk_size, file_size))
    else:
        chunks.append((0, file_size))

    return chunks


def process_file_chunk(filename: str, chunk: tuple, usr_str: str) -> pd.DataFrame:
    """
    Processes the specified chunk of a file.

    Args:
        filename: *.txt file path as the first argument.
        chunk: file chunk in bytes expressed as a tuple (ex.: (0, 1024)).
        usr_str: a string containing a single word.

    Returns:
        A list of Pandas DataFrames containing
        match ratios of user and text strings.

    """

    with open(filename, 'r', encoding="utf-8") as file:
        contents = file.read(chunk[1] - chunk[0])

    content_matrix = create_string_match_matrix(contents, usr_str)

    return content_matrix


def get_cl_args(argv: list) -> argparse.Namespace:
    """
    Parses command line arguments.

    Args:
        argv: a list containing command line arguments.

    Returns:
        An argparse.Namespace object.

    """

    parser = argparse.ArgumentParser(description='DESCRPTION',
                                     epilog='EXAMPLE USAGE')

    parser.add_argument('-f', '--filename', action='store', required=True,
                        type=str, metavar='', help='Text file name (*.txt)')

    parser.add_argument('-w', '--word', action='store', required=True,
                        type=str, metavar='',
                        help='A word to search (should not contain spaces)')

    return parser.parse_args(argv[1:])


def handle_user_input(usr_input: argparse.Namespace) -> None:
    """
    Validates user input expressed as command line arguments.

    Args:
        usr_input: an argparse.Namespace object.

    Returns:
        None.

    """

    try:
        with open(usr_input.filename, 'r', encoding="utf-8") as file:
            file.read(5)

    except FileNotFoundError:
        print('Check the file location and (or) file name. The file cannot be empty either.')
        sys.exit()


def main(argv: list) -> int:
    """
    Main program call.

    Args:
        argv: a list containing command line arguments.

    Returns:
        An integer representing system exit code.

    """

    # sys.args are parsed and checked for validity
    args = get_cl_args(argv)
    file_name = args.filename
    usr_string = args.word
    handle_user_input(args)

    # a file is divided into chunks for Multiprocessing
    chunk_list = file_chunks(file_name)

    # chunks are processed
    with multiprocessing.Pool(processes=os.cpu_count()) as pool:
        matrix_list = pool.starmap(process_file_chunk,
                                   itertools.product([file_name], chunk_list, [usr_string]))

    # results are printed out
    unique_words = get_results(matrix_list)
    for word in unique_words:
        print(word)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
